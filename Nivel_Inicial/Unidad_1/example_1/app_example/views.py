from django.shortcuts import render
from django.http import HttpResponse


def index(request):
    # Example for 'Adding templates to our webpage' in Apuntes.txt
    params = {'site_name': 'Online example'}

    return render(request, 'app_example/index.html', params)


"""
def index(request):
    # Example for 'Creating a new app for our webpage' in Apuntes.txt
    return HttpResponse("Hello world")
"""
