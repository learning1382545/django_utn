from django.contrib import admin
from app_example.models import Product, Category

# Register your models here.


class ProductAdmin(admin.ModelAdmin):
    fields = ['category', 'deploy_date', 'product', 'image']


admin.site.register(Product, ProductAdmin)
admin.site.register(Category)
# admin.site.register(Product)
