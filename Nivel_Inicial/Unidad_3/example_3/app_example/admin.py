from django.contrib import admin
from app_example.models import Product, Category

# Register your models here.


class ProductInLine(admin.TabularInline):
    model = Product
    extra = 0


class CategoryAdmin(admin.ModelAdmin):
    inlines = [ProductInLine]


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    # Including elements displayed in the product
    # fields = ['category', 'deploy_date', 'product', 'image']

    # Diving into sections
    fieldsets = [
        ("Relation", {"fields": ["category"]}),
        (
            "General info",
            {
                "fields": ["deploy_date", "product", "state", "image"]
            },
        ),
    ]

    # Including fields when displayed in list of products
    list_display = ["product", "deploy_date", "product_type", "image", "upper_case_name"]

    # Ordering through specified items
    ordering = ["-deploy_date"]

    # Filtering through specified items
    list_filter = ("product", "deploy_date",)

    # Including search
    search_fields = ("product", "state",)

    # Including link to edit specified field
    list_display_links = ("product", "deploy_date",)

    @admin.display(description='Name')
    def upper_case_name(self, obj):
        return ("%s %s" % (obj.product, obj.state)).upper()


# admin.site.register(Product, ProductAdmin)
admin.site.register(Category, CategoryAdmin)

# admin.site.register(Category)
# admin.site.register(Product)
