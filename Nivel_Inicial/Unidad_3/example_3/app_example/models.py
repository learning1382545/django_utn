from django.db import models
from django.utils.html import format_html


class Category(models.Model):
    name = models.CharField(max_length=100, db_index=True)
    slug = models.SlugField(max_length=100, db_index=True)

    def __str__(self):
        return '%s' % self.name


class Product(models.Model):
    # Product state
    template = 'Template'
    published = 'Published'
    collected = 'Collected'
    PRODUCT_APPROVED = (
        (template, 'Template'),
        (published, 'Published'),
        (collected, 'Collected'),
    )
    state = models.CharField(max_length=10, choices=PRODUCT_APPROVED, default='Template (Default value)')

    product = models.CharField(max_length=200)
    deploy_date = models.DateTimeField('Deploy date')
    image = models.ImageField(upload_to="product/%Y/%m/%d", blank=True, null=True)
    # category = models.ManyToManyField(Category)

    # Generate new variable for 1-N relations in database
    category = models.ForeignKey(
        Category, blank=False, null=True, on_delete=models.CASCADE
    )

    def product_type(self):
        # Colors selected with 'adobe kuler'
        if self.state == "Collected":
            return format_html('<span style="color: #f00;">{}</span>', self.state,)
        elif self.state == "Template":
            return format_html('<span style="color: #f0f;">{}</span>', self.state, )
        elif self.state == "Published":
            return format_html('<span style="color: #099;">{}</span>', self.state, )

    def __str__(self):
        return str(self.product) + " --- " + str(self.deploy_date)
