# Generated by Django 3.2.5 on 2023-12-01 21:28

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='UserData',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(blank=True, default='default/default.png', null=True, upload_to='product/%Y/%m/%d')),
                ('name', models.CharField(max_length=50)),
                ('last_name', models.CharField(max_length=50)),
                ('date_time', models.DateField(blank=True, null=True)),
                ('country', models.CharField(blank=True, max_length=30)),
                ('province', models.CharField(blank=True, max_length=40)),
                ('city', models.CharField(blank=True, max_length=40)),
                ('address', models.CharField(blank=True, max_length=80)),
                ('postal_code', models.CharField(blank=True, max_length=50)),
                ('phone', models.CharField(blank=True, max_length=30)),
                ('mobile', models.CharField(blank=True, max_length=30)),
                ('document', models.CharField(blank=True, max_length=30)),
                ('cuit', models.CharField(blank=True, max_length=30)),
                ('user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
