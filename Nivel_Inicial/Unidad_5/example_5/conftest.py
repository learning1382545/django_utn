import pytest
from app_example.models import Product
from app_example.models import Category
import datetime
from faker import Faker

fake = Faker()


@pytest.fixture()
def create_product(db):
    my_product = Product(product="Product 4", deploy_date=datetime.datetime.now())
    my_product.save()

    return my_product


@pytest.fixture()
def create_product_factory(db):
    category_1 = Category(name="Cat1", slug="cat1")
    category_1.save()

    def create_product(state: str = "Template", product: str = "Product 1", deploy_date=datetime.datetime.now(),
                       image: str = "path", category=category_1):
        my_product = Product(state=state, product=product, deploy_date=deploy_date, image=image, category=category)
        my_product.save()
        return my_product

    return create_product


@pytest.fixture()
def product(db, create_product_factory):
    return create_product_factory("Template", fake.name(), fake.date(), fake.file_path())
