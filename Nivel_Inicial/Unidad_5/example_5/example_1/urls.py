from django.conf import settings
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('app_example', include('app_example.urls')),
    path('accounts/', include('registration.backends.default.urls')),
    path('admin/', admin.site.urls),
    path('users/', include('users.urls')),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += path('__debug__/', include(debug_toolbar.urls)),
