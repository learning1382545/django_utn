import pytest
from app_example.models import Product
import datetime


@pytest.mark.django_db
def test_change_state(create_product):
    print("\nChanging state on a product")
    assert create_product.product == "Product 4"


@pytest.mark.django_db
def test_create_product():
    my_product = Product(product="Product 3", deploy_date=datetime.datetime.now())
    my_product.save()
    print("\n" + str(my_product.product))

    assert my_product.product == "Product 3"


@pytest.mark.django_db
def test_change_state_2(product):
    print("\nChanging state_2 test")
    print(product.state)
    print(product.product)
    assert product.state == "Template"
