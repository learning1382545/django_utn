from django.urls import path
from app_example import views

urlpatterns = [
    path('', views.index, name='index'),
]
