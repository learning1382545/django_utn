import pytest


@pytest.mark.skip(reason="this is a skip test")
def test_test():
    assert 1 == 1


def test_test2():
    assert 1 == 1


@pytest.mark.marca1
def test_test3():
    assert 1 == 1


# -----------------------------------

@pytest.fixture
def fixture_1():
    return 3


@pytest.mark.fixture_tests
def test_test4(fixture_1):
    variable = fixture_1
    assert variable == 1


# -----------------------------------

@pytest.fixture(scope="session")
def fixture_2():
    return 1


@pytest.mark.fixture_tests
def test_test5(fixture_2):
    variable = fixture_2
    assert variable == 1


# -----------------------------------

@pytest.fixture
def fixture_3():
    print("\nSetup process!!!\n")
    yield 1
    print("\nTeardown process!!\n")


@pytest.mark.setup_teardown
def test_test6(fixture_3):
    variable = fixture_3
    assert variable == 1
