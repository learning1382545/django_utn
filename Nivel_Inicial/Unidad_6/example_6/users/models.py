from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
import time


class UserData(models.Model):
    user = models.ForeignKey(User, blank=False, null=True, on_delete=models.CASCADE)
    image = models.ImageField(upload_to="product/%Y/%m/%d", default="default/default.png", blank=True, null=True)
    name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    date_time = models.DateField(blank=True, null=True)
    country = models.CharField(max_length=30, blank=True)
    province = models.CharField(max_length=40, blank=True)
    city = models.CharField(max_length=40, blank=True)
    address = models.CharField(max_length=80, blank=True)
    postal_code = models.CharField(max_length=50, blank=True)
    phone = models.CharField(max_length=30, blank=True)
    mobile = models.CharField(max_length=30, blank=True)
    document = models.CharField(max_length=30, blank=True)
    cuit = models.CharField(max_length=30, blank=True)

    def __str__(self):
        return self.user.username
